PYNO-IP SCRIPT
==============

Python script to update DNS name using www.no-ip.org.
The current IP of the machine where the script is run is associated to the specified hostname.

Basic usage (with logging):

.. code-block:: 

   $ pyno-ip -d "hostname.no-ip.org" -u "username" -p "password"

Disable logging:

.. code-block:: 

   $ pyno-ip -d "hostname.no-ip.org" -u "username" -p "password" -l false

Try the following for a brief explanation of the parameters:

.. code-block:: 

   $ pyno-ip -h

or:

.. code-block:: 
   
   $ pyno-ip --help

The command can be conveniently used in a cron job on machines running UNIX.
Don't exceed with the update frequency or no-ip.org could block your user!

Acknowledgments
---------------
This script is based on the following: http://snipplr.com/view/35917/
