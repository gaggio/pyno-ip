#!/usr/bin/python

import logging
import logging.handlers
import argparse

def create_logger():
    #OPEN LOG FILE
    logger = logging.getLogger('pyno-ip')
    hdlr = logging.handlers.RotatingFileHandler('pyno-ip.log', maxBytes=200000, backupCount=2)
    formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
    hdlr.setFormatter(formatter)
    logger.addHandler(hdlr)
    logger.setLevel(logging.INFO)
    logger.info("No-IP Updater Script - Version 0.4 - 15-10-2014 - F. Gaggiotti")
    return logger

def main(domain, user, pswd, IP, logger=None):
    
    # Update by URL
    if logger is not None:
        logger.info('Updating Host Info at no-ip.com')
        
    try:
        # For Python 3.0 and later
        from urllib.request import urlopen, HTTPBasicAuthHandler, build_opener, install_opener
        from urllib.error import HTTPError
        auth_handler = HTTPBasicAuthHandler()
        auth_handler.add_password(realm='dynupdate.no-ip.com',
                                  uri='http://dynupdate.no-ip.com/nic/update',
                                  user=user,
                                  passwd=pswd)
        opener = build_opener(auth_handler)
        install_opener(opener)
        command = "http://dynupdate.no-ip.com/nic/update?hostname="+domain
        if IP is not None:
            command += "&myip="+IP
        petition = urlopen(command)
    except ImportError:
        # Fall back to Python 2's urllib
        from urllib import urlopen
        command = "http://"+user+":"+pswd+"@dynupdate.no-ip.com/nic/update?hostname="+domain
        if IP is not None:
            command += "&myip="+IP
        petition = urlopen(command)

    
    # Get status
    aux = petition.read()
    if logger is not None:
        if "good" in str(aux):
            logger.info("DNS hostname update successful.")
        if "nochg" in str(aux):
            logger.info("IP address is current, no update performed.")
        if "nohost" in str(aux):
            logger.info("Hostname supplied does not exist under specified account.")
        if "badauth" in str(aux):
            logger.info("Invalid username password combination.")
        if "badagent" in str(aux):
            logger.info("Client disabled.")
        if "!donator" in str(aux):
            logger.info("An update request was sent including a feature that is not available to that particular user such as offline options.")
        if "abuse" in str(aux):
            logger.info("Username is blocked due to abuse.")
        if "911" in str(aux):
            logger.info("A fatal error on No-IP server. Retry the update no sooner 30 minutes.")


if __name__ == '__main__':
    # Get the arguments
    parser = argparse.ArgumentParser(description='www.no-ip.org python updater script')
    parser.add_argument("-l", "--logging", dest="log_flag", default="true",
                        choices=['true','false'],
                        help="Flag to activate logging (default: true)")
    #TODO: change next arguments do that it is not needed to enter between ""
    parser.add_argument("-d", "--domain", dest="domain", required=True,
                        help="Domain name to update (must exist in no-ip.org)")
    parser.add_argument("-u", "--user", dest="user", required=True,
                        help="No-ip user name")
    parser.add_argument("-p", "--password", dest="pswd", required=True,
                        help="No-ip password")
    parser.add_argument("-i", "--IP", dest="IP", required=False, help="IP number to associate to the address (default: current IP)", default=None)
    args = parser.parse_args()

    if args.log_flag=="true":
        logger = create_logger()
        main(args.domain, args.user, args.pswd, args.IP, logger)
    else:
        main(args.domain, args.user, args.pswd, args.IP)

